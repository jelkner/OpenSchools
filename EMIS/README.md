# Educational Management Information Systems (EMIS)

The term [educational management information systems](https://en.wikipedia.org/wiki/Education_management_information_system)
(EMIS) appears to have origniated in the
[United Nations Sustainable Development Goals](https://en.wikipedia.org/wiki/Sustainable_Development_Goals)
and more specifically, in
[SDG 4](https://en.wikipedia.org/wiki/Sustainable_Development_Goal_4) concening
quality education.

It seems to essentially be a rebranding of the term
[student information system](https://en.wikipedia.org/wiki/Student_information_system)
and as often happens with these tech rebrandings, makes effective searches
more challenging.  This section will document both names.

## Free Software EMIS / SIS Systems

- [RosarioSIS](https://www.rosariosis.org/)
- [OpenEMIS](https://www.openemis.org/)
